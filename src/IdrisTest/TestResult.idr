module IdrisTest.TestResult

import IdrisTest.ListShow

public export
data TestResult = ||| a passing test with module name, function name, and 
                  ||| message
                  Pass String String String
                | ||| a failing test with a module name, function name, and 
                  ||| message
                  Fail String String String

||| Formats given data as a JSON string.                                    
total
toJsonFromStrings : String -> String -> String -> String -> String
toJsonFromStrings moduleName functionName message status = 
  """
  {
      "moduleName" : "\{ moduleName }",
      "functionName" : "\{ functionName }",
      "message" : "\{ message }",
      "status" : "\{ status }"
  }
  """

||| Formats test result as a JSON string.
export total
toJson : TestResult -> String
toJson (Pass moduleName functionName message) = 
  toJsonFromStrings moduleName functionName message "✅ Pass"
toJson (Fail moduleName functionName message) = 
  toJsonFromStrings moduleName functionName message "❌ Fail"
  
export total
toJsons : List TestResult -> String
toJsons testResults = 
  """
  [
  \{  ListShow.showList (map toJson testResults) ", \n" } 
  ]
  """
