module IdrisTest.ListShow

||| Returns a string representing the list formatted by the separator.
export total
showList : List String -> String -> String
showList Nil _ = ""
showList (item :: Nil) _ = item --last item gets no separator afterward
showList (item :: items) separator = 
  item ++ separator ++ showList items separator 

