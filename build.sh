#!/usr/bin/env nix-shell
#! nix-shell -i bash --pure
#! nix-shell -p bash cacert chez git idris2

idris2 --build idris-test.ipkg
