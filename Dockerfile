from nixos/nix:master
copy src/ /home/src/
copy build.sh /home/
copy idris-test.ipkg /home/
workdir /home/
run ls
run ./build.sh
