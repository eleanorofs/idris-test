# idris-test
## A Simple Unit Testing Utility for Idris

## Goals: fast feedback and debugging for developers

While the Idris compiler can eliminate large categories of possible bugs, there
will always be logical errors which can't be type checked away. It's possible
that better developers than me are pretty comfortable a compiler + integration 
testing setup and don't feel the need to unit test, but for those who want it, 
this library may be able to help shift those issues left in the development
process. 

## Features: simple test setup using a standard Idris main function. 

Importantly, this is a testing *library* not a testing *framework*. All this is
is a convenient library for defining pass / fail assertions, evaluating the, 
and converting the results to JSON. What you do with that information beyond
that is up to you. 
